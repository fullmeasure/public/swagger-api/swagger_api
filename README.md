# Swagger::Api

A gem to make swagger documentation easier to create

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'swagger-api'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install swagger-api

## Usage

Create a swagger.yml and place it in you gem directory (see swagger.yml in root directory for an example)

    $ bundle exec rake swagger:api

This will create

## Format of swagger.yml

```yaml
info:
  version: 0.0.1
  title: FooService
  description: Controls the CRUD of Foo
servers: # List of your API endpoints
  test: # Rails.env
    url: https://foo.example.com/api/foobar
controllers:
  # This first format is the simplest and uses just the schema and validators to define the api
  - name: FooController # controller class
    model: Foo # model class associated with controller
    actions: # Optional: field to define which actions this endpoint responds to
      # the following two fields (only, except) are mutually exclusive. Choose one.
      only:
      - index
      - update
      except:
      - update
    columns: # Optional field to specify which params the api responds to. When using this specify at least one of the optional subfields.
      # You must include at least one of the following 4 fields

      # The following two fields (only, except) are mutually exclusive. Choose one.
      only:
      - column2
      - column3
      except:
      - column3

      virtual: # Defines virtual attributes. These do not exist in the associated model but are used by the controller.
      - name: virtual1
        schema:
          type: integer
          format: intg64
          minimum: 1 # this field is only for integer types

      required: # Defines additional fields as required even if there is not a validator associated with the field
      - column4
      - virtual1

  - name: CustomsController
    # Custom api json. You can use this gem to generate the base files from an existing model/controller and then modify as needed.
    custom_model_file: api/custom_model.json
    custom_path_file: api/custom_paths.json
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/fullmeasure/swagger-api/swagger-api. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Swagger::Api project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/swagger-api/blob/master/CODE_OF_CONDUCT.md).
