require 'rails_helper'

RSpec.describe SwaggerApi do
  it 'has a version number' do
    expect(SwaggerApi::VERSION).not_to be nil
  end
  describe 'output' do
    let(:expected_json) { File.read('./spec/swagger/expected.json').chomp }
    it 'integration' do
#      File.write('./spec/swagger/expected.json', SwaggerApi::Generator.new.prettify)
      expect(SwaggerApi::Generator.new.prettify).to eq expected_json
    end
  end
end
