# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do
  enable_extension "plpgsql"

  create_table "sti_parents", force: :cascade do |t|
    t.string "name"
    t.boolean "bool"
    t.string "type"
    t.float "number"
    t.integer "model_id"
    t.string "email"
    t.integer "count"
    t.string "encrypted_child1"
    t.string "encrypted_child1_iv"
    t.string "encrypted_child2"
    t.string "encrypted_child2_iv"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "models", force: :cascade do |t|
    t.string "column1"
    t.string "column2"
    t.string "column3"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "show_only_models", force: :cascade do |t|
    t.string "column2"
  end
end
