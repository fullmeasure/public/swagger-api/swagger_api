class StiParent < ApplicationRecord
  belongs_to :model
  validates :model, presence: true
end
