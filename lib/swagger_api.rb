# frozen_string_literal: true

require 'swagger_api/version'
require 'rails'

module SwaggerApi
  class Generator
    def prettify
      JSON.pretty_generate(JSON.parse(json))
    end

    def json
      create.to_json
    end

    def create
      @config ||= {
        openapi: '3.0.0',
        security: [{ api_key: [] }],
        info: info,
        servers: [{ url: server_url }],
        paths: Paths.new(controllers: config.controllers).create,
        components: components
      }
    end

    def components
      {
        responses: responses,
        schemas: Components.new(controllers: config.controllers).create,
        requestBodies: RequestBodies.new(controllers: config.controllers).create,
        securitySchemes: security_schemes
      }
    end

    def config
      @yaml_config ||= JSON.parse(YAML.load_file("#{Rails.root}/config/swagger.yml").to_json, object_class: OpenStruct)
    end

    def security_schemes
      {
        api_key: {
          type: 'apiKey',
          name: 'Authorization',
          in: 'header'
        }
      }
    end

    def responses
      @responses ||= default_responses
    end

    def default_responses
      {}.merge(default_not_found_response).merge(default_unauthorized_response).merge(default_bad_request_response)
    end

    def default_not_found_response
      {
        NotFound: {
          description: 'The specified resource was not found',
          content: {
            'application/json; charset=utf-8' => {
              schema: {
                type: 'string',
                example: 'Not Found'
              }
            }
          }
        }
      }
    end

    def default_unauthorized_response
      {
        Unauthorized: {
          description: 'Unauthorized',
          content: {
            'application/json; charset=utf-8' => {
              schema: {
                type: 'string',
                example: 'Not Authorized'
              }
            }
          }
        }
      }
    end

    def default_bad_request_response
      {
        BadRequest: {
          description: 'Bad Request',
          content: {
            'application/json; charset=utf-8' => {
              schema: {
                example: ['The field name is invalid.', 'The id must be present'],
                type: 'array',
                items: {
                  type: 'string'
                }
              }
            }
          }
        }
      }
    end

    def info
      {
        version: config.info.version,
        title: config.info.title,
        description: config.info.description
      }
    end

    def server_url
      return unless config.servers.respond_to?(Rails.env)
      config.servers.send(Rails.env).url
    end
  end
end
require 'swagger_api/railtie'

require 'swagger_api/actions'
require 'swagger_api/concerns/columns'
require 'swagger_api/concerns/sti_schema'
require 'swagger_api/column_schema'
require 'swagger_api/component_schema'
require 'swagger_api/components'
require 'swagger_api/operations/base'
require 'swagger_api/operations/create'
require 'swagger_api/operations/delete'
require 'swagger_api/operations/index'
require 'swagger_api/operations/show'
require 'swagger_api/operations/update'
require 'swagger_api/paths'
require 'swagger_api/request_bodies'
