# frozen_string_literal: true

module SwaggerApi
  module Operations
    class Index < Base
      def headers
        {
          'x-total' => {
            schema: {
              type: :integer
            },
            description: 'total results available'
          },
          'x-link-next' => {
            schema: {
              type: :string
            },
            description: 'uri for next page of results'
          }
        }
      end

      def parameters
        columns.map do |column|
          next if column.name.start_with?('encrypted_') || column.name.end_with?('_iv')
          {
            name: column.name,
            in: 'query',
            required: false,
            description: "#{column.name} of #{model.name}",
            schema: SwaggerApi::ColumnSchema.new(column: column).create
          }
        end.compact
      end

      def success_response
        success = super
        success['200'][:headers] = headers
        success
      end

      def readable_action
        'list'
      end

      def error_responses
        super.reject do |error_response|
          %w[404 422].include?(error_response.keys.first)
        end
      end
    end
  end
end
