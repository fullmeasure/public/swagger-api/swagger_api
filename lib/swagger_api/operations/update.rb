# frozen_string_literal: true

module SwaggerApi
  module Operations
    class Update < Base
      def create
        create = super
        create[:requestBody] = request_body
        create
      end

      def success_response
        {
          '303' => {
            description: "#{readable_action} #{model_name.downcase}'s information",
            headers: {
              Location: {
                schema: {
                  type: :string
                }
              }
            }
          }
        }
      end
    end
  end
end
