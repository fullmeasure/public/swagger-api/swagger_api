# frozen_string_literal: true

module SwaggerApi
  module Operations
    class Base
      include ActiveAttr::Model
      include SwaggerApi::Concerns::Columns
      include SwaggerApi::Concerns::StiSchema

      attr_accessor :controller, :action

      def create
        {
          summary: "#{readable_action} #{model_name}",
          description: "#{readable_action} #{model_name.downcase}'s information",
          parameters: parameters,
          responses: responses,
          tags: [model_name]
        }
      end

      def parameters
        [
          {
            name: 'id',
            in: 'path',
            description: "ID of #{model_name}",
            required: true,
            schema: {
              type: :integer,
              format: :int64,
              minimum: 1
            }
          }
        ]
      end

      def request_body
        {
          '$ref' => "#/components/requestBodies/#{model_name}"
        }
      end

      def responses
        return @responses unless @responses.nil?
        @responses = success_response
        error_responses.each do |error_response|
          @responses.merge!(error_response)
        end
        @responses
      end

      def success_response
        {
          '200' => {
            description: "#{readable_action} #{model_name.downcase}'s information",
            content: {
              'application/json; charset=utf-8' => {
                schema: schema(model)
              }
            }
          }
        }
      end

      def error_responses
        [
          {
            '404' => { '$ref' => '#/components/responses/NotFound' }
          },
          {
            '401' => { '$ref' => '#/components/responses/Unauthorized' }
          },
          {
            '422' => { '$ref' => '#/components/responses/BadRequest' }
          }
        ]
      end

      def readable_action
        @readable_action ||= self.class.name.demodulize.downcase
      end

      def model_name
        @model_name ||= controller.model
      end

      def model
        @model ||= model_name.constantize
      end
    end
  end
end
