# frozen_string_literal: true

module SwaggerApi
  module Operations
    class Create < Base
      def create
        create = super
        create[:requestBody] = request_body
        create.delete(:parameters)
        create
      end

      def success_response
        {
          '303' => {
            description: "#{readable_action} #{model_name.downcase}'s information",
            headers: {
              Location: {
                schema: {
                  type: :string
                }
              }
            }
          }
        }
      end

      def error_responses
        super.reject do |error_response|
          %w[404].include?(error_response.keys.first)
        end
      end
    end
  end
end
