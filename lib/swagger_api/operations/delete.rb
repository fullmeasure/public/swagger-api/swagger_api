# frozen_string_literal: true

module SwaggerApi
  module Operations
    class Delete < Base
      def success_response
        {
          '204' => {
            description: "#{readable_action} #{model_name.downcase}'s information"
          }
        }
      end

      def error_responses
        super.reject do |error_response|
          %w[422].include?(error_response.keys.first)
        end
      end
    end
  end
end
