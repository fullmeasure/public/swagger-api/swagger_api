# frozen_string_literal: true

module SwaggerApi
  module Operations
    class Show < Base
      def error_responses
        super.reject do |error_response|
          %w[422].include?(error_response.keys.first)
        end
      end
    end
  end
end
