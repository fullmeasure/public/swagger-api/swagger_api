# frozen_string_literal: true

module SwaggerApi
  class ColumnSchema
    include ActiveAttr::Model
    attr_accessor :column

    def create
      schema = default_schema
      if column.type == :integer
        schema[:minimum] = if column.name.to_s.ends_with?('id')
                             1
                           else
                             0
                           end
      end
      schema
    end

    def default_schema
      {
        type: type_from_column,
        format: format_from_column
      }
    end

    def type_from_column
      if %i[datetime date time].include?(column.type)
        :string
      elsif %i[float double].include?(column.type)
        :number
      else
        column.type
      end
    end

    def format_from_column
      case column.type
      when :datetime
        'date-time'
      when :integer
        :int64
      else
        if column.name.to_s == 'email'
          :email
        else
          column.type
        end
      end
    end
  end
end
