# frozen_string_literal: true

namespace :swagger do
  desc 'Create a local version of the swagger api json file'
  task swagger_api: [:environment] do
    generator = SwaggerApi::Generator.new
    File.open('api.yml', 'w+') do |file|
      file.write(YAML.dump(JSON.parse(generator.json)))
    end
    File.open('api.json', 'w+') do |file|
      file.write(generator.prettify)
    end
  end
end
