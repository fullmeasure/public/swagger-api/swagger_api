# frozen_string_literal: true

module SwaggerApi
  class Components
    include ActiveAttr::Model
    include Concerns::StiSchema

    attr_accessor :controllers

    def create
      return @components unless @components.nil?
      @components = {}
      controllers.each do |controller|
        if controller.custom_model_file.nil?
          @components.merge!(create_model_components_from_controller(controller))
        else
          @components.merge!(custom_json(controller.custom_model_file))
        end
      end
      @components
    end

    def custom_json(custom_model_file)
      file = File.read(custom_model_file)
      JSON.parse(file)
    end

    def create_model_components_from_controller(controller)
      if sti?(controller.model)
        sti_models(controller.model.constantize)
      else
        {
          controller.model => ComponentSchema.new(controller: controller).create
        }
      end
    end

    def sti_models(model_klass)
      model_klass.descendants.map do |klass|
        [klass.name, ComponentSchema.new(controller: OpenStruct.new(model: klass.name)).create]
      end.to_h
    end
  end
end
