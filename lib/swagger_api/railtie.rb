# frozen_string_literal: true

require 'rails'

#:nocov:#
module SwaggerApi
  class Railtie < Rails::Railtie
    rake_tasks do
      load 'swagger_api/tasks/swagger.rake'
    end
  end
end
#:nocov:#
