# frozen_string_literal: true

module SwaggerApi
  class RequestBodies
    include ActiveAttr::Model
    include SwaggerApi::Concerns::StiSchema

    attr_accessor :controllers

    def create
      request_bodies = {}
      controllers.each do |controller|
        request_bodies[controller.model] = request_body(controller)
      end
      request_bodies
    end

    def request_body(controller)
      {
        content: {
          'application/json' => {
            schema: schema(controller.model.try(:safe_constantize) || controller.model)
          }
        },
        description: "#{controller.model} attribute",
        required: true
      }
    end
  end
end
