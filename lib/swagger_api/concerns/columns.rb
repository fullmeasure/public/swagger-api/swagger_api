# frozen_string_literal: true

module SwaggerApi
  module Concerns
    module Columns
      extend ActiveSupport::Concern

      included do
        def columns
          filter_except_columns(filter_only_columns(model.columns))
        end

        def filter_only_columns(columns)
          if controller.columns.try(:only).present?
            columns.select do |column|
              controller.columns.only.include?(column.name)
            end
          else
            columns
          end
        end

        def filter_except_columns(columns)
          if controller.columns.try(:except).present?
            columns.reject do |column|
              controller.columns.except.include?(column.name)
            end
          else
            columns
          end
        end
      end
    end
  end
end
