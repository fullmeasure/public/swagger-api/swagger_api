# frozen_string_literal: true

module SwaggerApi
  module Concerns
    module StiSchema
      extend ActiveSupport::Concern

      included do
        def sti?(model)
          model_klass = model.try(:safe_constantize) || model
          return false if model_klass.is_a?(String) || model_klass.nil?
          Rails.application.eager_load!
          model_klass.descendants.count != 0
        end

        def schema(model)
          if sti?(model)
            {
              oneOf: model.descendants.map do |klass|
                { '$ref' => "#/components/schemas/#{klass.name}" }
              end
            }
          else
            {
              '$ref' => "#/components/schemas/#{model.try(:name) || model}"
            }
          end
        end
      end
    end
  end
end
