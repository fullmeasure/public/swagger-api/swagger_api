# frozen_string_literal: true

module SwaggerApi
  class ComponentSchema
    include ActiveAttr::Model
    include Concerns::Columns
    attr_accessor :controller

    def create
      {
        required: required,
        properties: properties.merge(virtual_properties),
        type: 'object'
      }
    end

    private

    def required
      controller.try(:columns).try(:required) ||
        columns.map(&:name).map { |name| name.gsub(/^encrypted_/, '') } & clean_required_attributes
    end

    def clean_required_attributes
      required_attributes.map do |attribute|
        if model.reflect_on_all_associations(:belongs_to).map(&:name).map(&:to_s).include?(attribute)
          "#{attribute}_id"
        else
          attribute
        end
      end.compact.uniq.map(&:to_s)
    end

    def required_attributes
      model.validators.map do |validator|
        validator.attributes if validator.is_a?(ActiveRecord::Validations::PresenceValidator)
      end.compact.flatten.uniq.map(&:to_s)
    end

    def absence_attributes
      @absence_attributes ||= model.validators.map do |validator|
        validator.attributes if validator.is_a?(ActiveRecord::Validations::AbsenceValidator)
      end.compact.flatten.uniq.map(&:to_s)
    end

    def properties
      columns.map do |column|
        next if column_should_skip column.name
        attribute_name = column.name.gsub(/^encrypted_/, '')
        [attribute_name, ColumnSchema.new(column: column).create]
      end.compact.to_h
    end

    def virtual_properties
      (controller.try(:columns).try(:virtual) || []).map do |property|
        [property.name, property.schema.to_h]
      end.to_h
    end

    def column_should_skip(column_name)
      column_name.end_with?('_iv') || absence_attributes.include?(column_name)
    end

    def model
      controller.model.constantize
    end
  end
end
