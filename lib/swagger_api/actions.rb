# frozen_string_literal: true

require 'active_attr'

module SwaggerApi
  class Actions
    include ActiveAttr::Model

    validate :validate_actions
    attr_accessor :controller

    def all!
      raise errors.full_messages unless valid?
      return only_actions unless only_actions.blank?
      defined_actions
    end

    def defined_actions
      restful_actions - except_actions
    end

    def only_actions
      controller.actions.try(:only) || []
    end

    def except_actions
      controller.actions.try(:except) || []
    end

    def restful_actions
      %w[index show create update delete]
    end

    def validate_actions
      errors.add(:base, "`actions` must include at least one of #{restful_actions}") if restful_actions.blank?
      errors.add(:base, "`actions` can only include one of #{restful_actions}. #{(defined_actions - restful_actions)} are not allowed") unless extra_actions.blank?
    end

    def extra_actions
      defined_actions - restful_actions
    end
  end
end
