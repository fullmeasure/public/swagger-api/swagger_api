# frozen_string_literal: true

module SwaggerApi
  class Paths
    include ActiveAttr::Model
    attr_accessor :controllers

    def create
      return @paths unless @paths.nil?
      @paths = {}
      controllers.each do |controller|
        if controller.custom_path_file.nil?
          @paths.merge!(paths(controller))
        else
          @paths.merge!(custom_json(controller))
        end
      end
      @paths
    end

    private

    def paths(controller)
      paths = {}
      paths[route(controller)] = {}
      paths["#{route(controller)}{id}/"] = {}
      collection_paths(paths, controller)
      member_paths(paths, controller)
      paths
    end

    def collection_paths(paths, controller)
      paths[route(controller)][:get] = SwaggerApi::Operations::Index.new(controller: controller).create if action?(controller, 'index')
      paths[route(controller)][:post] = SwaggerApi::Operations::Create.new(controller: controller).create if action?(controller, 'create')
    end

    def member_paths(paths, controller)
      show_path(paths, controller)
      update_path(paths, controller)
      delete_path(paths, controller)
      clean_out_delete_path(paths, controller)
    end

    def show_path(paths, controller)
      paths["#{route(controller)}{id}/"][:get] = SwaggerApi::Operations::Show.new(controller: controller).create if action?(controller, 'show')
    end

    def update_path(paths, controller)
      paths["#{route(controller)}{id}/"][:put] = SwaggerApi::Operations::Update.new(controller: controller).create if action?(controller, 'update')
    end

    def delete_path(paths, controller)
      paths["#{route(controller)}{id}/"][:delete] = SwaggerApi::Operations::Delete.new(controller: controller).create if action?(controller, 'delete')
    end

    def clean_out_delete_path(paths, controller)
      paths.delete route(controller) if paths[route(controller)].blank?
      paths.delete "#{route(controller)}{id}/" if paths["#{route(controller)}{id}/"].blank?
    end

    def route(controller)
      "/#{controller.name.demodulize.underscore.gsub('_controller', '')}/"
    end

    def custom_json(controller)
      file = File.read(controller.custom_path_file)
      JSON.parse(file)
    end

    def actions(controller)
      SwaggerApi::Actions.new(controller: controller).all!
    end

    def action?(controller, action_name)
      actions(controller).include?(action_name)
    end
  end
end
